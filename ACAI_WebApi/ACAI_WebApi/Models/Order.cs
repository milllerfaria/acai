﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ACAI_WebApi.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int Size { get; set; }
        [Required]
        public int Flavor { get; set; }
        public int PreparationTime { get; set; }
        public int FinalPrice { get; set; }
        public ICollection<Personalization> Personalizations { get; set; }
    }
}
