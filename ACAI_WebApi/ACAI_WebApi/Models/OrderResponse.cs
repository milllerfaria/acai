﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACAI_WebApi.Models
{
    public class OrderResponse
    {
        public int Id { get; set; }
        public string Size { get; set; }   
        public string Flavor { get; set; }
        public int PreparationTime { get; set; }
        public int FinalPrice { get; set; }
        public List<String> Personalizations { get; set; }
    }
}
