﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACAI_WebApi.Models
{
    public interface IOrderRepository
    {
        void Add(Order order);
        IEnumerable<Order> GetAll();
        Order Find(int id);
        void Remove(int id);
        void Update(Order order);
    }
}
