﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACAI_WebApi.Models
{
    public class Flavors
    {
        public string Description { get; set; }
        public int Time { get; set; }
    }
}
