﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACAI_WebApi.Models
{
    public class Sizes
    {
        public string Description { get; set; }
        public int Time { get; set; }
        public int Price { get; set; }
    }
}
