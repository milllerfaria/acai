﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ACAI_WebApi.Models
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options) : base(options){ }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Personalization> Personalizations { get; set; }
    }
}
