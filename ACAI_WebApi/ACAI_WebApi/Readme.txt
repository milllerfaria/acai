POST /api/order

send:  
{  
	"size": 1,
	"flavor": 1
}

response:
{
    "id": 1,
    "size": "Pequeno (300ml) R$10,00",
    "flavor": "Morango",
    "preparationTime": 5,
    "finalPrice": 10,
    "personalizations": []
}

send:
{  
	"size": 2,
	"flavor": 1
}
response:
{
    "id": 2,
    "size": "Medio (500ml) R$13,00",
    "flavor": "Morango",
    "preparationTime": 7,
    "finalPrice": 13,
    "personalizations": []
}


POST /api/personalization

send:
{  
	"orderId": 1,
	"option": 1
}

response - ID:
1

send:
{  
	"orderId": 2,
	"option": 2
}

response - ID:
2

GET /api/order/1

response:
{
    "id": 1,
    "size": "Pequeno (300ml) R$10,00",
    "flavor": "Morango",
    "preparationTime": 5,
    "finalPrice": 10,
    "personalizations": [
        "Granola"
    ]
}

GET /api/order/2
{
    "id": 2,
    "size": "Medio (500ml) R$13,00",
    "flavor": "Morango",
    "preparationTime": 10,
    "finalPrice": 16,
    "personalizations": [
        "Paçoca R$3,00"
    ]
}