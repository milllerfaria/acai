﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ACAI_WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ACAI_WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PersonalizationController : InjectedController
    {

        public PersonalizationController(OrderContext context) : base(context){}

        [HttpGet]
        public ICollection<Personalization> GetAll()
        {
            return _context.Personalizations.ToList();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var item = await _context.Personalizations.FirstOrDefaultAsync(t => t.Id == id); ;
            if (item == default(Personalization))
            {
                return NotFound();
            }
            return new ObjectResult(item);

        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Personalization p)
        {
            if (p == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (p.Option <= 0 || p.Option > 3)
            {
                return BadRequest("Invalid option value");
            }
        
            var order = await _context.Orders.FindAsync(p.OrderId);
            if (order == null)
            {
                ModelState.AddModelError("Order ID", $"Order {p.OrderId} does not exist");
                return BadRequest(ModelState);
            }
            await _context.Personalizations.AddAsync(p);

            await _context.SaveChangesAsync();
            return Ok(p.Id);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var todo = await _context.Personalizations.FirstOrDefaultAsync(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }
            var entity = await _context.Personalizations.FirstAsync(t => t.Id == id);
            _context.Personalizations.Remove(entity);
            await _context.SaveChangesAsync(); ;
            return new NoContentResult();
        }
    }
}

