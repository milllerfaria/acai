﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ACAI_WebApi.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ACAI_WebApi.Controllers
{   
    [Route("api/[controller]")]
    public class OrderController : InjectedController
    {
        Dictionary<int, Sizes> SizesDict;
        Dictionary<int, Flavors> FlavorsDict;
        Dictionary<int, PersonalizationValues> PersonalizationValuesDict;
        public OrderController(OrderContext context) : base(context){
            //TODO : Load this values from database
            SizesDict = new Dictionary<int, Sizes>()
            {
                {1, new Sizes{ Description = "Pequeno (300ml) R$10,00", Price = 10, Time = 5    } },
                {2, new Sizes{ Description = "Medio (500ml) R$13,00",   Price = 13, Time = 7    } },
                {3, new Sizes{ Description = "Grande (700ml) R$15,00",  Price = 15, Time = 10   } },
            };
            FlavorsDict = new Dictionary<int, Flavors>()
            {
                {1, new Flavors{ Description = "Morango", Time = 0 } },
                {2, new Flavors{ Description = "Banana", Time = 0 } },
                {3, new Flavors{ Description = "Kiwi", Time = 5 } },
            };
            PersonalizationValuesDict = new Dictionary<int, PersonalizationValues>()
            {
                {1, new PersonalizationValues{ Description = "Granola", Price = 0, Time = 0} },
                {2, new PersonalizationValues{ Description = "Paçoca R$3,00", Price = 3, Time = 3} },
                {3, new PersonalizationValues{ Description = "Leite Ninho", Price = 3, Time = 0} },
            };
        }

        [HttpGet]
        public ICollection<Order> GetAll()
        {
            return _context.Orders.ToList();
        }

        [HttpGet("{id}", Name = "GetOrder")]
        public async Task<IActionResult> GetById(int id)
        {
            var item = await _context.Orders.FirstOrDefaultAsync(t => t.Id == id); ;
            if (item == default(Order))
            {
                return NotFound();
            }

            var pre = await _context.Personalizations.Where(i => i.OrderId == item.Id).ToListAsync();
            item.Personalizations = pre;
            
            
            return new ObjectResult(FillOrderResponse(item, pre));

        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Order order)
        {
            if(order == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (order.Flavor <= 0 || order.Flavor > 3)
            {
                return BadRequest("Invalid flavor value");
            }
            else if (order.Size <= 0 || order.Size > 3)
            {
                return BadRequest("Invalid size value");
            }
      
            order.PreparationTime = SizesDict[order.Size].Time + FlavorsDict[order.Flavor].Time;
            order.FinalPrice = SizesDict[order.Size].Price;
        
            await _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();
            return Ok(FillOrderResponse(order, null));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var todo = await _context.Orders.FirstOrDefaultAsync(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }
            var entity = await _context.Orders.FirstAsync(t => t.Id == id);
             _context.Orders.Remove(entity);
            await _context.SaveChangesAsync(); ;
            return new NoContentResult();
        }

        private OrderResponse FillOrderResponse(Order item, List<Personalization> pre)
        {
            OrderResponse Or = new OrderResponse();
            Or.Id = item.Id;
            Or.Size = SizesDict[item.Size].Description;
            Or.Flavor = FlavorsDict[item.Flavor].Description;
            Or.PreparationTime = item.PreparationTime;
            Or.FinalPrice = item.FinalPrice;
            Or.Personalizations = new List<string>();
            if (pre != null)
            {
                foreach (var p in pre)
                {
                    Or.Personalizations.Add(PersonalizationValuesDict[p.Option].Description);
                    Or.PreparationTime += PersonalizationValuesDict[p.Option].Time;
                    Or.FinalPrice += PersonalizationValuesDict[p.Option].Price;
                }
            }
            return Or;
        }
    }

    public class InjectedController : ControllerBase
    {
        protected readonly OrderContext _context;

        public InjectedController(OrderContext context)
        {
            _context = context;
        }
    }
}
